package com.repository.mealsapp.ui.meals.state

import com.repository.mealsapp.model.MealsResponseCategory

sealed class MealsState {
    object Empty : MealsState()
    data class Result(val categories: List<MealsResponseCategory>) : MealsState()
    data class Error(val errorMsg: String) : MealsState()
}
