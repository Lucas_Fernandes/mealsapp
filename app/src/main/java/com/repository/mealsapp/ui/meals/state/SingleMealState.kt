package com.repository.mealsapp.ui.meals.state

import com.repository.mealsapp.model.MealsResponseCategory

sealed class SingleMealState {
    object Empty : SingleMealState()
    data class Meal(val meal: MealsResponseCategory?) : SingleMealState()
}
