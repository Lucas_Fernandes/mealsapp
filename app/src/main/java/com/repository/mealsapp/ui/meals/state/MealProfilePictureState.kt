package com.repository.mealsapp.ui.meals.state

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

enum class MealProfilePictureState(val color: Color, val size: Dp, val borderWidth: Dp) {
    Normal(color = Color.Magenta, size = 120.dp, borderWidth = 4.dp),
    Expanded(color = Color.Green, size = 200.dp, borderWidth = 2.dp),
}