package com.repository.mealsapp.ui.meals

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.repository.mealsapp.ui.details.MealDetailsScreen
import com.repository.mealsapp.ui.details.MealsDetailsViewModel
import com.repository.mealsapp.ui.meals.MainActivity.Companion.DESTINATION_MEALS_LIST
import com.repository.mealsapp.ui.meals.MainActivity.Companion.DESTINATION_MEAL_DETAILS
import com.repository.mealsapp.ui.meals.MainActivity.Companion.DESTINATION_MEAL_DETAILS_ARG
import com.repository.mealsapp.ui.meals.state.MealsState
import com.repository.mealsapp.ui.meals.state.SingleMealState
import com.repository.mealsapp.ui.theme.MealsAppTheme

class MainActivity : ComponentActivity() {

    private val viewModel: MealsCategoriesViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.interact(MealsIntent.GetMeals)

        setContent {
            val state = viewModel.state.collectAsState().value
            MealsAppTheme {
                MealsApp(
                    state = state,
                    interactFunction = viewModel::interact
                )
            }
        }
    }

    companion object {
        const val DESTINATION_MEALS_LIST = "destination_meals_list"
        const val DESTINATION_MEAL_DETAILS = "destination_meal_details"
        const val DESTINATION_MEAL_DETAILS_ARG = "meal_category_id"
    }
}

@Composable
private fun MealsApp(state: MealsState, interactFunction: (MealsIntent) -> Unit) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = DESTINATION_MEALS_LIST) {
        composable(route = DESTINATION_MEALS_LIST) {
            MealsCategoriesScreen(
                interact = interactFunction,
                state = state,
            ) { navigationMealId ->
                navController.navigate("$DESTINATION_MEAL_DETAILS/$navigationMealId")
            }
        }

        composable(
            route = "$DESTINATION_MEAL_DETAILS/{$DESTINATION_MEAL_DETAILS_ARG}",
            arguments = listOf(navArgument(DESTINATION_MEAL_DETAILS_ARG) {
                type = NavType.StringType
            })
        ) {
            val viewModel: MealsDetailsViewModel = viewModel()
            viewModel.mealState.collectAsState().value.let {
                when (it) {
                    SingleMealState.Empty -> {}
                    is SingleMealState.Meal -> it.meal?.let { meal -> MealDetailsScreen(meal) }
                }
            }
        }
    }
}