package com.repository.mealsapp.ui.meals

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ContentAlpha
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentAlpha
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.repository.mealsapp.R
import com.repository.mealsapp.model.MealsResponseCategory
import com.repository.mealsapp.ui.meals.state.MealsState
import com.repository.mealsapp.ui.theme.MealsAppTheme

@Composable
fun MealsCategoriesScreen(
    interact: (MealsIntent) -> Unit,
    state: MealsState? = null,
    onCategoryClicked: (String) -> Unit
) {
    when (state) {
        MealsState.Empty -> interact(MealsIntent.Empty)
        is MealsState.Error -> {}
        is MealsState.Result -> MealsCategoriesContent(
            state.categories,
            onCategoryClicked = onCategoryClicked
        )

        else -> {}
    }
}

@Composable
private fun MealsCategoriesContent(
    categoriesList: List<MealsResponseCategory>,
    onCategoryClicked: (String) -> Unit
) {
    LazyColumn(contentPadding = PaddingValues(16.dp)) {
        items(categoriesList) { meal ->
            MealCategory(
                meal = meal,
                onCategoryClicked = onCategoryClicked
            )
        }
    }
}

@Composable
private fun MealCategory(
    meal: MealsResponseCategory,
    onCategoryClicked: (String) -> Unit
) {
    var isExpanded by remember { mutableStateOf(false) }

    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 2.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp)
            .clickable { onCategoryClicked(meal.id) }
    ) {
        Row(modifier = Modifier.animateContentSize()) {// ANIMATES TE THE IMAGE SIZE
            AsyncImage(
                modifier = Modifier
                    .size(88.dp)
                    .padding(4.dp)
                    .align(Alignment.CenterVertically),
                model = meal.imageUrl,
                contentDescription = null
            )
            Column(
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .fillMaxWidth(0.8f)
                    .padding(16.dp)
            ) {
                Text(
                    text = meal.name,
                    style = MaterialTheme.typography.h6
                )
                CompositionLocalProvider(LocalContentAlpha provides ContentAlpha.medium) {
                    Text(
                        text = meal.description,
                        textAlign = TextAlign.Start,
                        style = MaterialTheme.typography.subtitle2,
                        overflow = TextOverflow.Ellipsis,
                        maxLines = if (isExpanded) 10 else 4
                    )
                }
            }
            Icon(
                modifier = Modifier
                    .padding(16.dp)
                    .align(Alignment.Bottom)
                    .clickable { isExpanded = !isExpanded },
                imageVector = if (isExpanded)
                    Icons.Filled.KeyboardArrowUp
                else
                    Icons.Filled.KeyboardArrowDown,
                contentDescription = stringResource(R.string.expand_row_icon_content_desc)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MealsAppTheme {
        MealsCategoriesScreen(
            interact = {},
            state = null,
            onCategoryClicked = {}
        )
    }
}