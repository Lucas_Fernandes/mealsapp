package com.repository.mealsapp.ui.details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.repository.mealsapp.repository.MealsRepository
import com.repository.mealsapp.ui.meals.MainActivity.Companion.DESTINATION_MEAL_DETAILS_ARG
import com.repository.mealsapp.ui.meals.state.SingleMealState
import kotlinx.coroutines.flow.MutableStateFlow


class MealsDetailsViewModel(
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    val mealState = MutableStateFlow<SingleMealState>(SingleMealState.Empty)

    private val repository: MealsRepository = MealsRepository.getInstance()
    init {
        val mealId = savedStateHandle.get<String>(DESTINATION_MEAL_DETAILS_ARG) ?: ""
        mealState.value = SingleMealState.Meal(repository.getMeal(mealId))
    }
}