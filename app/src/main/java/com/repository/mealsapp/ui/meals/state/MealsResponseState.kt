package com.repository.mealsapp.ui.meals.state

import com.repository.mealsapp.model.MealsResponseCategory

sealed class MealsResponseState {
    object Empty : MealsResponseState()
    data class SuccessResponse(val categories: List<MealsResponseCategory>) : MealsResponseState()
    data class ErrorResponse(val errorMsg: String) : MealsResponseState()
}