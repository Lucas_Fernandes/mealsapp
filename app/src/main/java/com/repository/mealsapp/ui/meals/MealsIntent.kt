package com.repository.mealsapp.ui.meals

sealed class MealsIntent {
    object GetMeals: MealsIntent()
    object Empty: MealsIntent()
}
