package com.repository.mealsapp.ui.meals

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.repository.mealsapp.repository.MealsRepository
import com.repository.mealsapp.ui.meals.state.MealsResponseState
import com.repository.mealsapp.ui.meals.state.MealsState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MealsCategoriesViewModel(
    private val repository: MealsRepository = MealsRepository.getInstance()
) : ViewModel() {

    val state: StateFlow<MealsState>
        get() = _state
    private var _state: MutableStateFlow<MealsState> = MutableStateFlow(MealsState.Empty)

    fun interact(intent: MealsIntent) {
        when (intent) {
            MealsIntent.GetMeals -> getMealsFromApi()
            MealsIntent.Empty -> _state.value = MealsState.Empty
        }
    }

    private fun getMealsFromApi() {
        viewModelScope.launch {
            with(repository) {
                getMeals()
                when (val repoState = state.value) {
                    MealsResponseState.Empty -> {}
                    is MealsResponseState.ErrorResponse -> {
                        _state.value = MealsState.Error(repoState.errorMsg)
                    }
                    is MealsResponseState.SuccessResponse -> {
                        _state.value = MealsState.Result(repoState.categories)
                    }
                }
            }
        }
    }
}