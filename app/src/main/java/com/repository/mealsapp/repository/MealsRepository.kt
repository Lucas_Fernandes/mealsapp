package com.repository.mealsapp.repository

import com.repository.mealsapp.model.MealsResponse
import com.repository.mealsapp.model.MealsResponseCategory
import com.repository.mealsapp.service.MealsApi
import com.repository.mealsapp.ui.meals.state.MealsResponseState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MealsRepository(private val webService: MealsApi = MealsApi()) {
    val state: StateFlow<MealsResponseState>
        get() = _state
    private val _state: MutableStateFlow<MealsResponseState> =
        MutableStateFlow(MealsResponseState.Empty)

    private var cachedMeals: List<MealsResponseCategory> = listOf()

    suspend fun getMeals() {
        val response = webService.getMeals()
        if (response.isSuccessful && response.body() != null) {
            cachedMeals = (response.body() as MealsResponse).categories
            _state.value = MealsResponseState.SuccessResponse(cachedMeals)
        } else {
            cachedMeals = emptyList()
            _state.value = MealsResponseState.ErrorResponse("Something went wrong.")
        }
    }

    fun getMeal(mealId: String): MealsResponseCategory? {
        return cachedMeals.firstOrNull {
            it.id == mealId
        }
    }

    companion object {
        @Volatile
        private var instance: MealsRepository? = null

        fun getInstance() = instance ?: synchronized(this) {
            instance ?: MealsRepository().also { instance = it }
        }
    }
}