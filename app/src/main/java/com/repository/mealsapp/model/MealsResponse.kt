package com.repository.mealsapp.model

import com.google.gson.annotations.SerializedName

data class MealsResponse(
    val categories: List<MealsResponseCategory>
)

data class MealsResponseCategory(
    @SerializedName("idCategory") val id: String,
    @SerializedName("strCategory") val name: String,
    @SerializedName("strCategoryThumb") val imageUrl: String,
    @SerializedName("strCategoryDescription") val description: String,
)