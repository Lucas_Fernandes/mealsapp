package com.repository.mealsapp.service

import com.repository.mealsapp.model.MealsResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MealsApi {
    private lateinit var api: MealsWebService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://www.themealdb.com/api/json/v1/1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        api = retrofit.create(MealsWebService::class.java)
    }

    suspend fun getMeals(): Response<MealsResponse> = api.getMeals()
}
