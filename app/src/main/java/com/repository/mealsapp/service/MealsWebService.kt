package com.repository.mealsapp.service

import com.repository.mealsapp.model.MealsResponse
import retrofit2.Response
import retrofit2.http.GET

interface MealsWebService {
    @GET("categories.php")
    suspend fun getMeals(): Response<MealsResponse>
}